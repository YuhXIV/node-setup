/*
const http = require('http');
const server = http.createServer((request, response) => {
    response.write('Hello Node World');
    response.end();
}).listen(8080);
*/

const express = require('express');
var app = express();
var path = require('path');
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/express_website.html'));
});

app.get('/hello.js', (req, res) => {
    res.sendFile(path.join(__dirname + '/hello.js'))
});
app.listen(process.env.PORT || 8080)