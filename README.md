# Node Setup

## Task 4 - Node server with http

- [x] Setup and run a node server with http
- [x] Use npm
- [x] The response should return “Hello Node World”

## Task 5 - Express website

- [x] Setup a node app with express
- [x] Serve an index.html file
- [x] The index file should have a ```<h1>``` tag with “Hello Node World!!” as the text and a ```<p>``` tag with text of your choice.
- [x] Add a JavaScript file
    - [x] Use ```console.log``` to display “Hello From an Express Site”
- [x] You will need to use the ```sendFile()``` function to server your index file

Deployment link(Heroku): https://simple-node-server-noroff.herokuapp.com/